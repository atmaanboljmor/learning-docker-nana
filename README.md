# Docker tutorial for beginner

Course link : <i>https://www.youtube.com/watch?v=3c-iBn73dDE&t=19s</i>

<details>
<summary>What is docker</summary>
<br/>

What is container: 
- A way to package application with all the necessary dependencies and configuration
- Protable artifact, easily shared and moved around
- Makes development and deployment more efficient

Where do containers live:
- Some storage = container repository (special type)
- Private repositories (company)
- Public respository for Docker (Dockerhub)

How container improved development process:
- Before the containers
  - Installation process different on each OS env
  - Many steps where something could go wrong
- After the containers
  - own isolated env
  - packaged with all needed configuration
  - one command to install the app
  - run same app with 2 different versions

How container improved deployment process:
- Before the containers
  - Developers team send source code and instruction document to operation team
  - Operation team will deploy it on the client server
  - configation on the server needed
  - textual guiade of deployment
  - dependency version conflicts
  - misunderstandings
- After the containers
  - Developer team and operation team share same docker file

</details>

---

<details>
<summary>What is container</summary>
<br/>

- layers of images
- Mostly linux base image, because small in size
- Application image on top
- Docker image vs Docker container <br/>
  IMAGE
  - The image is actual package
  - artifact, that can be moved around
  - not running  <br/>
  CONTAINER
  - actually start the application
  - container environment is created
  - running

</details>

---

<details>
<summary>Docker vs Virtual machine</summary>

- First understand OS
  - OS has 2 layers (Kernel, Applications)
- Docker
  - Docker is run on OS's = Application layer.
  - Size small
  - Fast
  - OS Compatibility
- VM
  - VM is run on OS's = Application layer + kernel layer.
  - Size big
  - Slow
  </details>

---

<details>
<summary>Docker installation</summary>

- See the official documentation

</details>

---

<details>
<summary>Docker main commands</summary>

- Pull image from resource

  - `docker pull` **_\<IMAGE NAME\>_**

- Run image or pull & run image
  - `docker run` **_\<IMAGE NAME\>_**
    - -d : detach mode
    - -p : port (e.g -p3000:3001; -p<host>:<container>)
    - --name : container name (--name container-name)
- Stop container
  - `docker stop` **_<CONTAINER NAME\>_**
- Start stopped container
  - `docker start` **_<CONTAINER NAME\>_**
- Running containers list
  - `docker ps`
    - -a : all running and stoped containers list
- Downloaded images list
  - `docker images`
    - -a : all downloaded images list

</details>

---

<details>
<summary>Debugging a Container</summary>

- See container log
  - `docker logs` **_<CONTAINER_ID>_**
- Enter container
  - `docker exet -it` **_<CONTAINER_ID>_** `/bin/bash`

</details>

---
